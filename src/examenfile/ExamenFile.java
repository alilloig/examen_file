/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenfile;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Álvaro Lillo Igualada @alilloig
 */
public class ExamenFile {

  public static void main(String[] args){
    
    //Ejercicio 1
    //a
    Scanner input = new Scanner(System.in);
    System.out.print("Introduzca el nombre del archivo a crear: ");
    File f = new File(input.next());
    try {
      f.createNewFile();
      System.out.println("Archivo "+f.getName()+" creado");
    } catch (IOException ex) {
      System.err.println("Error al crear el archivo "+f.getName()+": "+ex.toString());
    }
    //b
    boolean delete = false;
    if (!(f.isDirectory() && f.listFiles().length>1)){
      System.out.println("Borrando archivo "+f.getName());
      delete = f.delete();
    }
    if (delete) System.out.println("Archivo borrado");
    else System.out.println("No se pudo borrar el archivo");
    
    //Comprobación del ejercicio 2
    try {
      System.out.println("Using l3373r...");
      L3373R coder = new L3373R("secret");
      coder.to1337("Texto que va a manipularse, lAs vOcAlEs chOnIs dEbErIAn dEsApArEcEr");
      System.out.println(coder.from1337());
    } catch (IOException ex) {
      System.err.println("Error using l3373r: "+ex.toString());
    }
    
  }
}