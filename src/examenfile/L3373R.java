package examenfile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Clase que codifica y descodifica textos en ficheros.
 * Esta codificación se realiza sustituyendo las vocales por números de acuerdo a la
 * siguiente equivalencia: A - 4, E - 3, I - 1, 0 - 0.
 * @author Álvaro Lillo Igualada
 */
public class L3373R {
  private File f;
  private FileReader reader;
  private FileWriter writer;

  /**
   * Constructor de la clase encargado de inicializar el descriptor de fichero,
   * el lector de ficheros de texto y el escritor de ficheros de texto.
   * @param fileName El nombre del archivo con el que se va a trabajar.
   * @throws FileNotFoundException
   * @throws IOException 
   */
  public L3373R (String fileName) throws FileNotFoundException, IOException{
    this.f = new File(fileName);
    if (!f.exists())f.createNewFile();
    this.reader = new FileReader(f);
    this.writer = new FileWriter(f);
  }
  
  /**
   * Metodo que lee el texto del fichero y lo devuelve descódificado.
   * @return String con el texto del fichero modificado.
   * @throws IOException 
   */
  public String from1337 () throws IOException{
    int aux;
    String text = new String();
    //Leemos todos los caracteres del fichero y si son números los convertimos a letras
    //antes de guardarlos en la String que devolveremos
    do{
      aux = numberToLetter(reader.read());
      text = text + (char)aux;
    }while(aux!=-1);
    return text;
  }
  
  /**
   * Metodo que códifica una String y la escribe en un fichero
   * @param text Texto que se quiere codificar.
   * @return True si la operación se han completado con exito.
   * @throws IOException 
   */
  public boolean to1337 (String text) throws IOException{
    //Utilizando manipulación de Strings
    char [] aux = new char[text.length()];
    for(int i=0;i<text.length();i++){
      aux[i] = letterToNumber(text.charAt(i));
    }
    this.writer.write(aux);
    //Utilizando string.replace
    //this.writer.write(text.replace('a','4').replace('e','3').replace('i','1').replace('o','0'));
    //Cerramos el escritor
    writer.close();
    //Si ha llegado hasta este punto devolvemos true
    return true;
  }
  
  //Convierte los caracteres 4,3,1,0 en a,e,i,o (en codigo decimal)
  private int numberToLetter(int number){
    if (number == 52) return 97;
    if (number == 51) return 101;
    if (number == 49) return 105;
    if (number == 48) return 111;
    return number;
  }

  //Convierte los caracteres a,e,i,o en 4,3,1,0
  private char letterToNumber(char letter) {
    if (letter == 'a' || letter == 'A') return '4';
    if (letter == 'e' || letter == 'E') return '3';
    if (letter == 'i' || letter == 'I') return '1';
    if (letter == 'o' || letter == 'O') return '0';
    return letter;
  }
}